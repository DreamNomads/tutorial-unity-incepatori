﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SfarsitNivel : MonoBehaviour {

    public Transform drapel;
    public Vector3 pozitieFinal;
    public float vitezaDrapel = 3f;

    public Manager mn;

    Vector3 pozitieInitiala;
    Vector3 pozitieFinala;
    bool nivelTerminat;
    float procent;
    bool incarca;

	// Use this for initialization
	void Start () {
        pozitieInitiala = drapel.position;
        nivelTerminat = false;
        incarca = false;
        procent = 0;
        pozitieFinala = drapel.position + pozitieFinal;
	}
	
	// Update is called once per frame
	void Update () {
        

        if (nivelTerminat) {
            if (procent < 1f)
            {
                procent += Time.deltaTime * vitezaDrapel;
                drapel.position = Vector3.Lerp(pozitieInitiala, pozitieFinala, procent);
            }
            else {
                procent = 1;
                drapel.position = pozitieFinala;
                if (!incarca) {
                    StartCoroutine(IncarcaScena());
                    incarca = true;
                }
                //
            }
        }
	}

    IEnumerator IncarcaScena() {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Meniu");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Minge") {
            if (Manager.scor == mn.scorMaxim)
            {
                Debug.Log("Am terminat");
                nivelTerminat = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        OnTriggerEnter(other);
    }

    private void OnDrawGizmos()
    {
        
            
            Gizmos.color = Color.green;

        Gizmos.DrawCube(drapel.position + pozitieFinal, Vector3.one);
            
        
        
    }
}
