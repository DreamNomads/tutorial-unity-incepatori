﻿using UnityEngine;
using System.Collections;

public class Minge : MonoBehaviour {

	public float viteza = 10f;
	public float lungimeRaza=5f;

	public float putereDeSarit = 100f;

    public VirtualStick stick;

	Rigidbody rb;

	public bool poateSari=false;

    

    // Use this for initialization
    void Start () {
		rb = GetComponent<Rigidbody> ();
		if (rb == null) {
			gameObject.AddComponent<Rigidbody> ();
		}
		poateSari = false;
	}
	
	// Update is called once per frame
	void Update () {
        float miscOriz = stick.Orizontala();
		float miscVert = stick.Verticala();

		Vector3 miscare = new Vector3 (miscOriz, 0, miscVert);

		rb.AddForce (miscare * viteza);

		RaycastHit hit;
		Ray raza = new Ray (transform.position, Vector3.down);

		if (Physics.Raycast (raza, out hit, lungimeRaza)) {
			if (hit.collider.tag == "Sol") {
                //transform.parent = hit.collider.transform;
				poateSari = true;
                
			}
		} else {
			poateSari = false;
		}

		if (Input.GetButtonDown ("Jump") && poateSari) {
            //transform.parent = null;
			rb.AddForce (new Vector3 (0, putereDeSarit, 0));
			//poateSari = false;
		}
        
    }

   

    void OnDrawGizmos(){
		Gizmos.color = Color.red;
		Gizmos.DrawLine (transform.position, transform.position + Vector3.down * lungimeRaza);
	}

    
}
