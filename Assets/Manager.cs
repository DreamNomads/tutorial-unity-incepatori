﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour {

	public int scorMaxim=10;
	public static int scor;
	public Text textScor;
	public Text mesaj;

	public GameObject minge;
	public float inaltimeMinimaPermisa=-10f;

	public float timpPentruRestart = 2f;
	private float timpRastartare;

	private bool incepeRestartarea;
	private bool amRestartat = false;

	void Awake(){
		Time.timeScale = 1f;
		mesaj.gameObject.SetActive (false);
		incepeRestartarea = false;
		amRestartat = false;
		scor = 0;
	}
		
	
	// Update is called once per frame
	void Update () {
		textScor.text = "Scor: " + scor;
		/*if (scor == scorMaxim) {
			mesaj.text="Ai Castigat";
			mesaj.gameObject.SetActive (true);
		}*/

		if (incepeRestartarea) {
			StartCoroutine (Restart ());
			incepeRestartarea = false;
		}

		if (minge.transform.position.y < inaltimeMinimaPermisa) {
			//Time.timeScale = 0f;
			mesaj.text="Ai cazut!";
			mesaj.gameObject.SetActive (true);
			if (amRestartat == false) {
				incepeRestartarea = true;
				amRestartat = true;
			}
		}
	}

	IEnumerator Restart(){
		yield return new WaitForSeconds (timpPentruRestart);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}
}
