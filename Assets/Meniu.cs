﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Meniu : MonoBehaviour {

    public void Iesire() {
        Debug.Log("Am Iesit din joc");
        Application.Quit();
    }

    public void StartB()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
