﻿using UnityEngine;
using System.Collections;

public class CameraMea : MonoBehaviour {

	public Transform tinta;

	Vector3 distanta;

	// Use this for initialization
	void Start () {
		distanta = transform.position - tinta.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = tinta.position + distanta;
	}
}
