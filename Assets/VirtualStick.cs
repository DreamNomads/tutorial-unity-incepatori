﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VirtualStick : MonoBehaviour, IDragHandler,IPointerUpHandler,IPointerDownHandler {

    private Image suprafata;
    private Image analogStick;
    private Vector3 input;

	// Use this for initialization
	void Start () {
        suprafata = GetComponent<Image>();
        analogStick = transform.GetChild(0).GetComponent<Image>();
	}
	

    public virtual void OnDrag(PointerEventData ped) {
        Vector2 pozitie;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(suprafata.rectTransform, ped.position, ped.pressEventCamera, out pozitie)) {
            pozitie.x = pozitie.x / suprafata.rectTransform.sizeDelta.x;
            pozitie.y = pozitie.y / suprafata.rectTransform.sizeDelta.y;

            input = new Vector3(pozitie.x * 2, 0, pozitie.y * 2);
            input = (input.magnitude > 1) ? input.normalized : input;

            analogStick.rectTransform.anchoredPosition = new Vector3(input.x * (suprafata.rectTransform.sizeDelta.x / 3),
                                                                    input.z * (suprafata.rectTransform.sizeDelta.y / 3),
                                                                    0);
        }
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        input = Vector3.zero;
        analogStick.rectTransform.anchoredPosition = input;
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public float Orizontala() {
        if (input.x != 0)
        {
            return input.x;
        }
        else {
            return Input.GetAxis("Horizontal");
        }
    }

    public float Verticala()
    {
        if (input.z != 0)
        {
            return input.z;
        }
        else
        {
            return Input.GetAxis("Vertical");
        }
    }

}
