﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Autodistrugere : MonoBehaviour {

    public float timpAutodistrugere = 3f;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, timpAutodistrugere);
	}
	
	
}
