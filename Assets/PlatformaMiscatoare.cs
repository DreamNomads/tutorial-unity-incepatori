﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformaMiscatoare : MonoBehaviour {

    public float viteza = 3f;
    public Vector3[] listaPozitiiLocale;
    public float secundeAsteptat = 3f;

    public float marimePunct=1f;

    private Vector3[] listaPozitiiGlobale;
    private int pozitieActuala;
    private int pozitieUrmatoare;

    private float procent;

    Rigidbody rb;

    private Rigidbody rbMinge;
    private Vector3 distantaMinge;
    

    // Use this for initialization
    void Start () {

        rb = GetComponent<Rigidbody>();

        listaPozitiiGlobale = new Vector3[listaPozitiiLocale.Length];
        for (int i = 0; i < listaPozitiiLocale.Length; i++) {
            listaPozitiiGlobale[i] = transform.position + listaPozitiiLocale[i];
        }
        pozitieActuala = 0;
        transform.position = listaPozitiiGlobale[pozitieActuala];
        procent = 0;
	}

    IEnumerator Miscare() {
        if (pozitieActuala + 1 == listaPozitiiGlobale.Length)
        {
            pozitieUrmatoare = 0;
        }
        else {
            pozitieUrmatoare=pozitieActuala+1;
        }
        if (procent < 3f)
        {
            procent += Time.deltaTime * viteza;
            rb.position = Vector3.Lerp(listaPozitiiGlobale[pozitieActuala], listaPozitiiGlobale[pozitieUrmatoare], procent);
            yield return null;
        }
        else {
            procent = 0;
            rb.position = listaPozitiiGlobale[pozitieUrmatoare];
            pozitieActuala = pozitieUrmatoare;
            yield return new WaitForSeconds(secundeAsteptat);
        }
        yield return null;
    }

	// Update is called once per frame
	void Update () {
        if (listaPozitiiGlobale.Length > 1)
        {
            StartCoroutine(Miscare());
        }
    }

    private void FixedUpdate()
    {
        if (rbMinge != null) {
            rbMinge.MovePosition(rb.position + distantaMinge);
        }
    }



    private void OnDrawGizmos()
    {
        for (int i = 0; i < listaPozitiiLocale.Length; i++) {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(transform.position + listaPozitiiLocale[i], marimePunct);
            Gizmos.color = Color.green;
            if (i < listaPozitiiLocale.Length - 1) {
                Gizmos.DrawLine(transform.position + listaPozitiiLocale[i], transform.position + listaPozitiiLocale[i + 1]);
            }
        }
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position + listaPozitiiLocale[0], transform.position + listaPozitiiLocale[listaPozitiiLocale.Length-1]);
    }

    private void OnCollisionEnter(Collision collision)
    {
        OnCollisionStay(collision);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.transform.tag == "Minge") {
            Transform minge = collision.collider.transform;
            rbMinge = minge.GetComponent<Rigidbody>();
            distantaMinge = minge.position - transform.position;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        rbMinge = null;
    }


}
