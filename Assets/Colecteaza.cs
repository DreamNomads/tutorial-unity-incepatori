﻿using UnityEngine;
using System.Collections;

public class Colecteaza : MonoBehaviour {

	public float rotatie= 45f;
    public GameObject particule;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (rotatie, rotatie, rotatie);
	}

	void OnTriggerEnter(Collider altcineva){
		if (altcineva.tag == "Minge") {
            Instantiate(particule, transform.position, Quaternion.identity);
			gameObject.SetActive (false);
			Manager.scor = Manager.scor + 1;
		}
	}
}
